const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const passport = require("passport");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//serve static files
app.use(express.static(path.join(__dirname, 'dist')));


//setup api routes
// app.use('/api', api);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/findATutor-ui/index.html'));
});

//set port
const PORT = process.env.PORT || 3001;
app.set('port', PORT);

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`))
